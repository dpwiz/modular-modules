{-# LANGUAGE UndecidableInstances #-} -- Eq/Ord/Show deriving

module Momo.ModSyntax
  ( Specification(..)
  , ModType(..)
  , TypeDecl(..)

  , Definition(..)
  , ModTerm(..)

  , substTypeDecl
  , substModType
  ) where

import Momo.CoreSyntax qualified as Core
import Momo.Ident (Ident)
import Momo.Path (Path)
import Momo.Subst (Subst)

data TypeDecl term = TypeDecl
  { kind     :: Core.Kind term
  , manifest :: Maybe (Core.Def term)
  }

deriving instance Core.EqTerm term => Eq (TypeDecl term)
deriving instance Core.OrdTerm term => Ord (TypeDecl term)
deriving instance Core.ShowTerm term => Show (TypeDecl term)

data ModType term
  = Signature [Specification term]
  | FunctorType Ident (ModType term) (ModType term)

deriving instance Core.EqTerm term => Eq (ModType term)
deriving instance Core.OrdTerm term => Ord (ModType term)
deriving instance Core.ShowTerm term => Show (ModType term)

data Specification term
  = ValueSig Ident (Core.Val term)
  | TypeSig Ident (TypeDecl term)
  | ModuleSig Ident (ModType term)

deriving instance Core.EqTerm term => Eq (Specification term)
deriving instance Core.OrdTerm term => Ord (Specification term)
deriving instance Core.ShowTerm term => Show (Specification term)

data ModTerm term
  = LongIdent Path
  | Structure [Definition term]
  | Functor Ident (ModType term) (ModTerm term)
  | Apply (ModTerm term) (ModTerm term)
  | Constraint (ModTerm term) (ModType term)

deriving instance Core.EqTerm term => Eq (ModTerm term)
deriving instance Core.OrdTerm term => Ord (ModTerm term)
deriving instance Core.ShowTerm term => Show (ModTerm term)

data Definition term
  = ValueStr Ident term
  | TypeStr Ident (Core.Kind term) (Core.Def term)
  | ModuleStr Ident (ModTerm term)

deriving instance Core.EqTerm term => Eq (Definition term)
deriving instance Core.OrdTerm term => Ord (Definition term)
deriving instance Core.ShowTerm term => Show (Definition term)

substTypeDecl
  :: forall term
  .  Core.CoreSyntax term
  => TypeDecl term
  -> Subst
  -> TypeDecl term
substTypeDecl decl sub =
  TypeDecl
    { kind =
        Core.substKind @term decl.kind sub
    , manifest =
        fmap
          ( \dty ->
              Core.substDef @term dty sub
          )
          decl.manifest
    }

substModType
  :: forall term
  .  Core.CoreSyntax term
  => ModType term
  -> Subst
  -> ModType term
substModType mty sub =
  case mty of
    Signature sg ->
      Signature $
        map (substSigItem sub) sg
    FunctorType ident mty1 mty2 ->
      FunctorType
        ident
        (substModType mty1 sub)
        (substModType mty2 sub)

substSigItem
  :: forall term
  .  Core.CoreSyntax term
  => Subst
  -> Specification term
  -> Specification term
substSigItem sub = \case
  ValueSig ident vty ->
    ValueSig ident (Core.substVal @term vty sub)
  TypeSig ident decl ->
    TypeSig ident (substTypeDecl decl sub)
  ModuleSig ident mty ->
    ModuleSig ident (substModType mty sub)
