module Momo.CoreSyntax
  ( CoreSyntax(..)

  , EqTerm
  , OrdTerm
  , ShowTerm
  ) where

import Momo.Subst (Subst)

class CoreSyntax term where
  type Val term
  type Def term
  type Kind term
  substVal :: Val term -> Subst -> Val term
  substDef :: Def term -> Subst -> Def term
  substKind :: Kind term -> Subst -> Kind term

type EqTerm term =
  ( Eq (Kind term)
  , Eq (Def term)
  , Eq (Val term)
  , Eq term
  )

type OrdTerm term =
  ( Ord (Kind term)
  , Ord (Def term)
  , Ord (Val term)
  , Ord term
  )

type ShowTerm term =
  ( Show (Kind term)
  , Show (Def term)
  , Show (Val term)
  , Show term
  )
