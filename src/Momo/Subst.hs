module Momo.Subst
  ( Subst
  , path
  ) where

import Data.Map qualified as Map

import Momo.Ident qualified as Ident
import Momo.Path (Path)
import Momo.Path qualified as Path

type Subst = Ident.Table Path

path :: Path -> Subst -> Path
path p sub =
  case p of
    Path.Ident i ->
      case Map.lookup i sub of
        Nothing ->
          p
        Just p' ->
          p'
    Path.Dot root field ->
      Path.Dot (path root sub) field
