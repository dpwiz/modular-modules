module Momo.ModTyping
  ( typeModule
  , ModTypingError(..)
  ) where

import Control.Monad (unless, when)
import Control.Monad.Catch (Exception, MonadThrow(throwM))
import Data.Foldable (for_)
import Data.Map.Strict qualified as Map
import Data.Text (Text)

import Momo.CoreTyping qualified as CT
import Momo.CoreSyntax qualified as Core
import Momo.Env (Env)
import Momo.Env qualified as Env
import Momo.Ident (Ident(..))
import Momo.Path (Path)
import Momo.Path qualified as Path
import Momo.ModSyntax qualified as Mod
import Momo.Subst (Subst)

data ModTypingError
  = CircularValue Text
  | CircularType Text
  | CircularModule Text
  | KindMismatch Text
  | UnmatchedSignatureComponent Text
  | ValueComponentsMismatch Text Text
  | TypeComponentsMismatch Text Text
  | ModuleTypeMismatch
  deriving (Eq, Ord, Show)

instance Exception ModTypingError

typeModule
  :: forall term m
  .  ( CT.CoreTyping term
     , MonadThrow m
     )
  => Env term
  -> Mod.ModTerm term
  -> m (Mod.ModType term)
typeModule env = \case
  Mod.LongIdent path ->
    Env.findModule path env >>=
      strengthenModType path

  Mod.Structure struct ->
    fmap Mod.Signature $
      typeStructure env [] struct

  Mod.Functor param mty body -> do
    checkModType env mty
    fmap (Mod.FunctorType param mty) $
      typeModule
        (Env.addModule param mty env)
        body

  Mod.Apply funct arg ->
    case arg of
      Mod.LongIdent path ->
        typeModule env funct >>= \case
          Mod.FunctorType param mty_param mty_res -> do
            mty_arg <- typeModule env arg
            modTypeMatch env mty_arg mty_param
            pure $ Mod.substModType mty_res (Map.singleton param path)
          _nonFunctor ->
            error "application of a non-functor"
      _nonPath ->
        error "application of a functor to a non-path"

  Mod.Constraint modl mty -> do
    checkModType env mty

    sig <- typeModule env modl
    modTypeMatch env sig mty

    pure mty

typeStructure
  :: ( CT.CoreTyping term
     , MonadThrow m
     )
  => Env term
  -> [Text]
  -> [Mod.Definition term]
  -> m [Mod.Specification term]
typeStructure env seen = \case
  [] ->
    pure []
  structItem : remaining -> do
    (seen', sigItem) <- typeDefinition env seen structItem
    next <- typeStructure (Env.addSpec sigItem env) seen' remaining
    pure (sigItem : next)

typeDefinition
  :: ( CT.CoreTyping term
     , MonadThrow m
     )
  => Env term
  -> [Text]
  -> Mod.Definition term
  -> m ([Text], Mod.Specification term)
typeDefinition env seen = \case
  Mod.ValueStr ident term -> do
    when (ident.name `elem` seen) $
      throwM $ CircularValue ident.name
    typ <- CT.typeTerm env term
    pure
      ( ident.name : seen
      , Mod.ValueSig ident typ
      )

  Mod.ModuleStr ident modl -> do
    when (ident.name `elem` seen) $
      throwM $ CircularModule ident.name
    sig <- typeModule env modl
    pure
      ( ident.name : seen
      , Mod.ModuleSig ident sig
      )

  Mod.TypeStr ident kind typ -> do
    when (ident.name `elem` seen) $
      throwM $ CircularType ident.name
    CT.checkKind env kind
    kind' <- CT.kindDefType env typ
    unless (CT.kindMatch env kind' kind) $
      throwM $ KindMismatch ident.name
    pure
      ( ident.name : seen
      , Mod.TypeSig ident Mod.TypeDecl
          { kind = kind
          , manifest = Just typ
          }
      )

checkModType
  :: ( CT.CoreTyping term
     , MonadThrow m
     )
  => Env term
  -> Mod.ModType term
  -> m ()
checkModType env = \case
  Mod.Signature sg ->
    checkSignature env mempty sg
  Mod.FunctorType param arg res -> do
    checkModType env arg
    checkModType (Env.addModule param arg env) res

checkSignature
  :: ( CT.CoreTyping term
     , MonadThrow m
     )
  => Env term
  -> [Text]
  -> [Mod.Specification term]
  -> m ()
checkSignature env seen = \case
  [] ->
    pure ()

  Mod.ValueSig ident vty : remaining -> do
    when (ident.name `elem` seen) $
      throwM $ CircularValue ident.name
    CT.checkValType env vty
    checkSignature env (ident.name : seen) remaining

  Mod.TypeSig ident decl : remaining -> do
    when (ident.name `elem` seen) $
      throwM $ CircularType ident.name
    CT.checkKind env decl.kind
    for_ decl.manifest \typ -> do
      kind' <- CT.kindDefType env typ
      unless (CT.kindMatch env kind' decl.kind) $
        throwM $ KindMismatch ident.name
    checkSignature
      (Env.addType ident decl env)
      (ident.name : seen)
      remaining

  Mod.ModuleSig ident mty : remaining -> do
    when (ident.name `elem` seen) $
      throwM $ CircularModule ident.name
    checkModType env mty
    checkSignature
      (Env.addModule ident mty env)
      (ident.name : seen)
      remaining

modTypeMatch
  :: forall term m
  . ( CT.CoreTyping term
    , MonadThrow m
    )
  => Env term
  -> Mod.ModType term
  -> Mod.ModType term
  -> m ()
modTypeMatch env mty1 mty2 =
  case (mty1, mty2) of
    (Mod.Signature sig1, Mod.Signature sig2) -> do
      (pairedComponents, subst) <- pairSignatureComponents sig1 sig2
      let extEnv = Env.addSignature sig1 env
      for_ pairedComponents $
        specificationMatch @term extEnv subst

    (Mod.FunctorType param1 arg1 res1, Mod.FunctorType param2 arg2 res2) -> do
      let subst = Map.singleton param1 (Path.Ident param2)
      let res1' = Mod.substModType res1 subst
      modTypeMatch env arg2 arg1
      modTypeMatch (Env.addModule param2 arg2 env) res1' res2

    _ ->
      throwM ModuleTypeMismatch

pairSignatureComponents
  :: (Core.CoreSyntax term, MonadThrow m)
  => [Mod.Specification term]
  -> [Mod.Specification term]
  -> m
    ( [ (Mod.Specification term, Mod.Specification term) ]
    , Subst
    )
pairSignatureComponents sig1 sig2 =
  case sig2 of
    [] ->
      pure ([], mempty)
    item2 : remaining2 -> do
      let
        findMatchingComponent = \case
          [] ->
            throwM $ UnmatchedSignatureComponent $
              case item2 of
                Mod.ValueSig ident2 _ ->
                  ident2.name
                Mod.TypeSig ident2 _ ->
                  ident2.name
                Mod.ModuleSig ident2 _ ->
                  ident2.name

          item1 : remaining1 ->
            case (item1, item2) of
              (Mod.ValueSig ident1 _, Mod.ValueSig ident2 _)
                | ident1.name == ident2.name ->
                    pure (ident1, ident2, item1)

              (Mod.TypeSig ident1 _, Mod.TypeSig ident2 _)
                | ident1.name == ident2.name ->
                    pure (ident1, ident2, item1)

              (Mod.ModuleSig ident1 _, Mod.ModuleSig ident2 _)
                | ident1.name == ident2.name ->
                    pure (ident1, ident2, item1)

              _ ->
                findMatchingComponent remaining1

      (ident1, ident2, item1) <- findMatchingComponent sig1
      (pairs, subst) <- pairSignatureComponents sig1 remaining2

      pure
        ( (item1, item2) : pairs
        , Map.insert ident2 (Path.Ident ident1) subst
        )

specificationMatch
  :: forall term m
  .  ( CT.CoreTyping term
     , MonadThrow m
     )
  => Env term
  -> Subst
  -> (Mod.Specification term, Mod.Specification term)
  -> m ()
specificationMatch env subst = \case
  (Mod.ValueSig ident1 vty1, Mod.ValueSig ident2 vty2) ->
    unless (CT.valTypeMatch env vty1 (Core.substVal @term vty2 subst)) $
      throwM $ ValueComponentsMismatch ident1.name ident2.name

  (Mod.TypeSig ident1 decl1, Mod.TypeSig ident2 decl2) -> do
    let decl2' = Mod.substTypeDecl @term decl2 subst :: Mod.TypeDecl term
    unless (typeDeclMatch @term env ident1 decl1 decl2') $
      throwM $ TypeComponentsMismatch ident1.name ident2.name

  (Mod.ModuleSig _ mty1, Mod.ModuleSig _ mty2) ->
    modTypeMatch env mty1 (Mod.substModType mty2 subst)

  _brokenPairing ->
    error "Invalid signature component pair"

typeDeclMatch
  :: forall term
  . CT.CoreTyping term
  => Env term
  -> Ident
  -> Mod.TypeDecl term
  -> Mod.TypeDecl term
  -> Bool
typeDeclMatch env ident decl1 decl2 = kindMatches && manifestMatches
  where
    kindMatches =
      CT.kindMatch env decl1.kind decl2.kind

    manifestMatches =
      case (decl1.manifest, decl2.manifest) of
        (_, Nothing) ->
          True

        (Just typ1, Just typ2) ->
          CT.defTypeEquiv env decl2.kind typ1 typ2

        (Nothing, Just typ2) ->
          CT.defTypeEquiv
            env
            decl2.kind
            ( CT.deftypeOfPath
                @term
                (Path.Ident ident)
                decl1.kind
            )
            typ2

strengthenModType
  :: ( CT.CoreTyping term
     , MonadThrow m
     )
  => Path
  -> Mod.ModType term
  -> m (Mod.ModType term)
strengthenModType path = \case
  Mod.Signature sg ->
    fmap Mod.Signature $
      traverse (strengthenSpec path) sg

  mty@Mod.FunctorType{} ->
    pure mty

strengthenSpec
  :: forall term m
  .  ( CT.CoreTyping term
     , MonadThrow m
     )
  => Path
  -> Mod.Specification term
  -> m (Mod.Specification term)
strengthenSpec path = \case
  item@Mod.ValueSig{} ->
    pure item

  Mod.TypeSig ident decl -> do
    let
      manifest' =
        case decl.manifest of
          Just ty ->
            ty
          Nothing ->
            CT.deftypeOfPath
              @term
              (Path.Dot path ident.name)
              decl.kind
    pure $
      Mod.TypeSig ident Mod.TypeDecl
        { kind     = decl.kind
        , manifest = Just manifest'
        }

  Mod.ModuleSig ident mty ->
    fmap (Mod.ModuleSig ident) $
      strengthenModType
        (Path.Dot path ident.name)
        mty
