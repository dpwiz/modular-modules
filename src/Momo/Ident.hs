module Momo.Ident
  ( Ident(..)

  , MonadStamp(..)
  , create

  , Table
  ) where

import Data.Map (Map)
import Data.Text (Text)

-- class Ident a where
--   create :: Text -> a
--   name :: a -> Text
--   equal :: a -> a -> Bool

-- class (Ident ident) => Table ident a where
--   emptyTable :: Table ident a
--   add :: ident -> a -> Table ident a -> Table ident a
--   find :: ident -> Table ident a -> Maybe a

data Ident = Ident
  { name  :: Text
  , stamp :: Integer
  }
  deriving (Show)

instance Eq Ident where
  a == b = a.stamp == b.stamp

instance Ord Ident where
  compare a b = compare a.stamp b.stamp

class (Monad m) => MonadStamp m where
  nextStamp :: m Integer

create :: MonadStamp m => Text -> m Ident
create name = do
  stamp <- nextStamp
  pure Ident{..}

type Table a = Map Ident a
