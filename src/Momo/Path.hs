module Momo.Path
  ( Path(..)
  , items
  ) where

import Data.Text (Text)

import Momo.Ident (Ident)
import Momo.Ident qualified as Ident

data Path
  = Ident
    { ident :: Ident
    } -- ^ module identifier
  | Dot
    { path  :: Path
    , field :: Text
    } -- ^ access to a module component
  deriving (Eq, Ord)

instance Show Path where
  show = show . items

items :: Path -> [Text]
items = go
  where
    go = \case
      Ident{ident} ->
        [ident.name]
      Dot{path, field} ->
        go path <> [field]
