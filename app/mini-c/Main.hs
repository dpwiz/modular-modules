module Main where

import Text.Show.Pretty (pPrint)

import MiniC.Test qualified as MiniC

main :: IO ()
main = do
  MiniC.testType >>= pPrint
